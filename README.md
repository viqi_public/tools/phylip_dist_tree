# phylip_dist_tree

A set of tools to generate trees based on distance matrixes, for manipulation in Python.
Uses FITCH and DRAWTREE from [PHYLIP](https://evolution.genetics.washington.edu/phylip/) by Joseph Felsenstein



## Getting started

This package requires prior installation of PHYLIP. In Ubuntu (18 & 20) and Gentoo (among possibly others), this can be installed using `apt`:

    sudo apt install phylip

## Install python package

    pip install .

## Python usage

```python
from phylip_dist_tree import maketree, drawtree

sim_mat_test=[['pred label', 'neg', 'pos1', 'pos2', 'pos3', 'pos4'],
    ['neg',  0.987, 0.0,   0.005, 0.001, 0.007],
    ['pos1', 0.0,   0.495, 0.266, 0.184, 0.054],
    ['pos2', 0.072, 0.247, 0.394, 0.144, 0.144],
    ['pos3', 0.001, 0.194, 0.142, 0.62,  0.044],
    ['pos4', 0.062, 0.053, 0.182, 0.048, 0.655]]

X0e, Y0e, X1e, Y1e, Xt, Yt, tlabels = maketree (sim_mat_test)
fig = drawtree(X0e, Y0e, X1e, Y1e, Xt, Yt, tlabels)
fig.show(config=dict(displayModeBar=False))
```
![image](/uploads/bf2648f4cfe09fada4ca437e1fddb574/image.png)
