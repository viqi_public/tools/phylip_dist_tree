import plotly
import plotly.graph_objects as go

import logging

def drawtree (X0e, Y0e, X1e, Y1e, Xt, Yt, tlabels):
	log = logging.getLogger(__name__ if __name__ != "__main__" else __file__.replace('.py', ''))
	save_log_level = log.level
	log.setLevel(logging.ERROR)

	fig = go.Figure()
	fig.add_trace(go.Scatter(x=Xt,
		y=Yt,
		mode='text',
		name='bla',
		text=tlabels,
		hoverinfo='none',
		textposition = 'top right',
		textfont=dict(
			family="sans serif",
			size=14,
			color="black"
			)
		)
	)
	for idx in range (len(X0e)):
		fig.add_shape(type="line",
			x0=X0e[idx], y0=Y0e[idx], x1=X1e[idx], y1=Y1e[idx],
			line=dict(color="RoyalBlue",width=3)
		)
	fig.update_layout(
		width = 584 * .5,
		height = 765 * .5,
		autosize = False,
		paper_bgcolor='rgba(0,0,0,0)',
	#    plot_bgcolor='rgba(0,0,0,0)',
		margin = dict (
			l=0, #left margin
			r=0, #right margin
			b=0, #bottom margin
			t=20, #top margin
		)
	)
	fig.update_xaxes(
		range = (0,584),
		fixedrange = True,
		showgrid   = False, # thin lines in the background
		zeroline   = False, # thick line at x=0
		visible    = False,  # numbers below

	  )
	fig.update_yaxes(
		range = (0,765),
		fixedrange=True,
		scaleanchor = "x",
		scaleratio = 1,
		showgrid   = False, # thin lines in the background
		zeroline   = False, # thick line at x=0
		visible    = False,  # numbers below
	  )
	fig.update_shapes(dict(xref='x', yref='y'))

	log.setLevel(save_log_level)
	return (fig)
