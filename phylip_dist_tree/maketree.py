import os, errno, re
from subprocess import Popen, PIPE, STDOUT, DEVNULL
from scipy.spatial.distance import pdist, squareform
import tempfile
import shutil
import csv


PHYLIP_PATH = os.path.dirname(__file__)

def maketree (sim_mat, tree_file = None, distmat_file = None, force_long = False):
	dist_mat = pdist ([x[1:] for x in sim_mat [1:]])
	sq_dist_mat = squareform (dist_mat)
	labels=[x[0] for x in sim_mat[1:]]

	def silentunlink(path):
		try:
			os.remove(path)
		except OSError as e: # this would be "except OSError, e:" before Python 2.6
			if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
				raise # re-raise exception if a different error occurred

	tmp_dir = tempfile.TemporaryDirectory()
	with open(os.path.join(tmp_dir.name, 'infile'), "w") as distmat:
		distmat.write ('{:5}\n'.format(len(sq_dist_mat)))
		for idx, label in enumerate (labels):
			distmat.write ('{:10} '.format(label[:10]))
			for dist in sq_dist_mat[idx]:
				distmat.write (' {:#7.5f}'.format (dist))
			distmat.write ('\n')
	if distmat_file:
		silentunlink (distmat_file)
		shutil.copyfile(os.path.join(tmp_dir.name, 'infile'), distmat_file)


	silentunlink (os.path.join(tmp_dir.name, 'outfile'))
	if len(sq_dist_mat) < 20 or force_long:
		# global rearrangements and scramble input order
		fitch_params = b"G\nJ\n5\n5\n2\n3\nY\n"
	elif len(sq_dist_mat) < 100:
		# global rearrangements only
		fitch_params = b"G\n2\n3\nY\n"
	else:
		# use defaults
		fitch_params = b"2\n3\nY\n"
	with Popen(['phylip', 'fitch'], stdin=PIPE, stdout=DEVNULL, cwd=tmp_dir.name) as fitch:
		fitch.communicate(fitch_params)
	silentunlink (os.path.join(tmp_dir.name, 'outfile'))

	if tree_file:
		silentunlink (tree_file)
		shutil.copyfile(os.path.join(tmp_dir.name, 'outtree'), tree_file)
	os.rename(os.path.join(tmp_dir.name, 'outtree'), os.path.join(tmp_dir.name, 'intree'))

	with Popen(['phylip', 'drawtree'], stdin=PIPE, stdout=DEVNULL, cwd=tmp_dir.name) as fitch:
		fitch.communicate(os.path.join(PHYLIP_PATH, 'font1').encode() + b"\nY\n")

	X0e, Y0e, X1e, Y1e, Xt, Yt, tlabels = [], [], [], [], [], [], []
	waitlabel = False
	with open(os.path.join(tmp_dir.name, 'plotfile')) as file:
		for line in file:
			line = line.rstrip()
			m = re.search(r'([0-9\.]+)\s+([0-9\.]+)\s+([0-9\.]+)\s+([0-9\.]+)\s+l$', line)
			if m:
				X0e.append (float(m.group(1)))
				X1e.append (float(m.group(3)))
				Y0e.append (float(m.group(2)))
				Y1e.append (float(m.group(4)))
			m = re.search (r'([0-9\.]+)\s+([0-9\.]+)\s+translate\s+[0-9\.]+\s+rotate$', line)
			if m:
				Xt.append (float(m.group(1)))
				Yt.append (float(m.group(2)))
				waitlabel = True
			m = re.search (r'\((.+)\)\s+show$', line)
			if m and waitlabel:
				tlabels.append (m.group(1))
				waitlabel = False
	try:
		tmp_dir.cleanup()
	except OSError:
		pass
	return (X0e, Y0e, X1e, Y1e, Xt, Yt, tlabels)
