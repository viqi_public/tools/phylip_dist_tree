import os
from setuptools import setup

HERE = os.path.abspath(os.path.dirname(__file__))
NAME = 'phylip_dist_tree'

setup(
	name=NAME,
	version='0.1.0',
	url='https://gitlab.com/viqi_public/tools/phylip_dist_tree.git',
	author='Ilya Goldberg',
	author_email='igg@viqi.org',
	description='Generate trees based on distance matrixes.',
	packages=["phylip_dist_tree"],
	install_requires=['scipy >= 1.5.2'],
	extras_require = {
		'plotly_trees':  ["plotly >= 5.5.0"],
	},
	include_package_data=True,
	setup_requires=['setuptools_scm'],
)
